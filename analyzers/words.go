package analyzers

import (
	"regexp"
	"strings"

	nospam "git.autistici.org/ai/nospam2"
	"git.autistici.org/ai/nospam2/util"
	"github.com/blevesearch/segment"
	"golang.org/x/text/unicode/norm"
	"mvdan.cc/xurls/v2"
)

const (
	RAW_WORDS = "raw_words"
	WORDS     = "words"

	minWordSize = 4
	maxWordSize = 26

	urlToken   = "URLPLACEHOLDER"
	urlTokenSp = " " + urlToken + " "
)

var (
	htmlTagRx = regexp.MustCompile(`<[^>]*>`)
	urlsRx    = xurls.Relaxed()
)

// Extract the list of words, in the order as they appear in the text.
// Drops HTML tags, and replaces all URLs with a placeholder token.
type rawWordAnalyzer struct{}

func (w *rawWordAnalyzer) Analyze(comment *nospam.Comment) interface{} {
	var words []string
	for _, txt := range []string{comment.Comment, comment.Author} {
		txt = maskURLs(cleanHTML(normUnicode(txt)))
		segmenter := segment.NewWordSegmenter(strings.NewReader(txt))
		for segmenter.Segment() {
			word := segmenter.Text()
			if (word == urlToken || (len(word) >= minWordSize && len(word) <= maxWordSize)) &&
				(segmenter.Type() != segment.None && segmenter.Type() != segment.Number) {
				words = append(words, word)
			}
		}
	}
	return words
}

// Aggregate unique words used in the text and count their occurrence.
type wordAnalyzer struct{}

func (w *wordAnalyzer) Analyze(comment *nospam.Comment) interface{} {
	rawWords, ok := comment.Data(RAW_WORDS).([]string)
	if !ok {
		return nil
	}
	words := util.NewStringCounter()
	for _, w := range rawWords {
		if w != urlToken {
			w = strings.ToLower(w)
		}
		words.Inc(w)
	}
	return words
}

// Normalize the UTF-8 representation.
func normUnicode(s string) string {
	return norm.NFKC.String(s)
}

// Drop all HTML tags.
func cleanHTML(s string) string {
	return htmlTagRx.ReplaceAllLiteralString(s, "")
}

// Replace all URLs with a placeholder (urlToken).
func maskURLs(s string) string {
	return urlsRx.ReplaceAllLiteralString(s, urlTokenSp)
}

func init() {
	nospam.RegisterAnalyzer(RAW_WORDS, &rawWordAnalyzer{})
	nospam.RegisterAnalyzer(WORDS, &wordAnalyzer{})
}
