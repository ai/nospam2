package analyzers

import (
	"regexp"
	"strings"

	nospam "git.autistici.org/ai/nospam2"
	"git.autistici.org/ai/nospam2/util"
	"mvdan.cc/xurls/v2"
)

const URLS = "urls"

type URLInfo struct {
	URLs     util.StringSet
	Emails   util.StringSet
	URLCount int
}

type urlAnalyzer struct{}

var (
	urlRx            = xurls.Relaxed()
	looksLikeEmailRx = regexp.MustCompile(`^[^:/]+@[^:/]+$`)
)

func (u *urlAnalyzer) Analyze(comment *nospam.Comment) interface{} {
	info := &URLInfo{
		URLs:   util.NewStringSet(),
		Emails: util.NewStringSet(),
	}

	// Check various text fields in the comment, and extract all
	// URLs and email addresses. xurls.Relaxed() returns both, so
	// we have to tell them apart ourselves.
	for _, txt := range []string{comment.Comment, comment.Author, comment.Email, comment.Link} {
		for _, m := range urlRx.FindAllString(txt, -1) {
			if looksLikeEmailRx.MatchString(m) {
				info.Emails.Add(strings.ToLower(m))
			} else {
				info.URLs.Add(m)
				info.URLCount++
			}
		}
	}

	return info
}

func init() {
	nospam.RegisterAnalyzer(URLS, &urlAnalyzer{})
}
