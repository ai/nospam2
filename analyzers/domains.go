package analyzers

import (
	"net"
	"net/url"
	"strings"
	"time"

	nospam "git.autistici.org/ai/nospam2"
	"git.autistici.org/ai/nospam2/util"
	"golang.org/x/net/publicsuffix"
)

const (
	DOMAINS          = "domains"
	TOPLEVEL_DOMAINS = "tl_domains"
	EMAIL_DOMAINS    = "email_domains"
)

// Extract unique domains (host names) from the list of URLs.
type domainAnalyzer struct{}

func (d *domainAnalyzer) Analyze(comment *nospam.Comment) interface{} {
	urls, ok := comment.Data(URLS).(*URLInfo)
	if !ok {
		return nil
	}

	domains := util.NewStringSet()
	for u := range urls.URLs {
		// URLs are potentially schema-less, so handle that
		// case by looking for :// in the string. We're just
		// going to add a fake http:// prefix, so we can still
		// use url.Parse to handle paths etc.
		if !strings.Contains(u, "://") {
			u = "http://" + u
		}

		parsed, err := url.Parse(u)
		if err != nil {
			continue
		}

		// Drop eventual port from the URL host.
		if parsed.Host != "" {
			domains.Add(withoutPort(parsed.Host))
		}
	}

	return domains
}

// Extract unique domains from the list of emails.
type emailDomainAnalyzer struct{}

func (d *emailDomainAnalyzer) Analyze(comment *nospam.Comment) interface{} {
	urls, ok := comment.Data(URLS).(*URLInfo)
	if !ok {
		return nil
	}

	domains := util.NewStringSet()
	for email := range urls.Emails {
		domain := email[strings.IndexByte(email, '@')+1:]
		domains.Add(domain)
	}

	return domains
}

// Compute the list of top-level-plus-one unique domains, where
// "top-level" is defined by a combination of publicsuffix TLDs and
// SURBL well-known hostings (wordpress.com etc).
type toplevelDomainAnalyzer struct{}

func (d *toplevelDomainAnalyzer) Analyze(comment *nospam.Comment) interface{} {
	domains, ok := comment.Data(DOMAINS).(util.StringSet)
	if !ok {
		return nil
	}

	tlds := util.NewStringSet()
	for domain := range domains {
		// Pass IP addresses as they are.
		if ip := net.ParseIP(domain); ip != nil {
			goto add
		}

		// Check SURBL's list of common top-level-domains that
		// have their own independent subdomains (large
		// hosting platforms etc).
		if tld, ok := twoLevelTLDs.Set().HasPrefix(domain); ok {
			domain = tldPlusOne(domain, tld)
			goto add
		}
		if tld, ok := threeLevelTLDs.Set().HasPrefix(domain); ok {
			domain = tldPlusOne(domain, tld)
			goto add
		}

		// As fallback, extract the effective-tld+1 from the
		// domain using the publicsuffix TLD list.
		if tldp1, err := publicsuffix.EffectiveTLDPlusOne(domain); err == nil {
			domain = tldp1
		}

	add:
		tlds.Add(domain)
	}

	return tlds
}

func withoutPort(s string) string {
	if h, _, err := net.SplitHostPort(s); err == nil {
		return h
	}
	return s
}

func tldPlusOne(domain, tld string) string {
	if domain == tld {
		return domain
	}
	r := strings.LastIndexByte(domain[:len(domain)-len(tld)-1], '.')
	return domain[r+1:]
}

// Keep a copy of surbl.org top-level domain lists, updated periodically.
var (
	twoLevelTLDs   *util.RemoteStringSet
	threeLevelTLDs *util.RemoteStringSet
)

func init() {
	nospam.RegisterAnalyzer(DOMAINS, &domainAnalyzer{})
	nospam.RegisterAnalyzer(EMAIL_DOMAINS, &emailDomainAnalyzer{})
	nospam.RegisterAnalyzer(TOPLEVEL_DOMAINS, &toplevelDomainAnalyzer{})

	twoLevelTLDs = util.NewRemoteStringSet(
		"https://www.surbl.org/tld/two-level-tlds",
		util.NewStringSetFromList(twoLevelTLDsDefault),
		86400*time.Second,
	)
	threeLevelTLDs = util.NewRemoteStringSet(
		"https://www.surbl.org/tld/three-level-tlds",
		util.NewStringSetFromList(threeLevelTLDsDefault),
		86400*time.Second,
	)

}
