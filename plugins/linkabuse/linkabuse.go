package linkabuse

import (
	"context"
	"net/url"
	"strings"

	nospam "git.autistici.org/ai/nospam2"
	"github.com/boltdb/bolt"
)

// Checks if the comment link appears again in the comment text.
type linkabusePlugin struct{}

func (p *linkabusePlugin) Name() string { return "linkabuse" }

func (p *linkabusePlugin) Init(_ map[string]interface{}, _ *bolt.DB) error { return nil }

func (p *linkabusePlugin) Train(ctx context.Context, comment *nospam.Comment) error { return nil }

func (p *linkabusePlugin) Test(ctx context.Context, comment *nospam.Comment) (float64, string, error) {
	// Parse the link URL if present.
	uri := comment.Link
	if !strings.Contains(uri, "://") {
		uri = "http://" + uri
	}

	parsed, err := url.Parse(uri)
	if err == nil && parsed.Host != "" {
		if strings.Contains(
			strings.ToLower(comment.Comment),
			strings.ToLower(parsed.Host)) {
			return 1, "link repeats in body", nil
		}
	}

	return 0, "", nil
}

func init() {
	nospam.RegisterPlugin(&linkabusePlugin{})
}
