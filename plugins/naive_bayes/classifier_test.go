package bayes

import (
	"io/ioutil"
	"os"
	"testing"

	nospam "git.autistici.org/ai/nospam2"
	"git.autistici.org/ai/nospam2/analyzers"
	"git.autistici.org/ai/nospam2/util"
	"github.com/boltdb/bolt"
)

func createTestClassifier(t *testing.T) (*Classifier, *bolt.DB, func()) {
	dir, err := ioutil.TempDir("", "")
	if err != nil {
		t.Fatal(err)
	}

	db, err := bolt.Open(dir+"/nospam.db", 0600, nil)
	if err != nil {
		t.Fatal(err)
	}

	c, err := createClassifier(db, 0.15)
	if err != nil {
		t.Fatal(err)
	}

	return c, db, func() {
		db.Close()
		os.RemoveAll(dir)
	}
}

func TestClassifier_LowLevelMethods(t *testing.T) {
	c, db, cleanup := createTestClassifier(t)
	defer cleanup()

	err := db.Update(func(tx *bolt.Tx) error {
		b := tx.Bucket(c.bucket)
		c.incCategoriesDocuments(b, "spam")
		c.incCategoriesDocuments(b, "spam")
		c.incToken(b, "spam", "foo", 4)
		return nil
	})
	if err != nil {
		t.Fatal(err)
	}

	err = db.View(func(tx *bolt.Tx) error {
		b := tx.Bucket(c.bucket)

		n := c.getCategoriesDocuments(b, "spam")
		if n != 2 {
			t.Errorf("getCategoriesDocuments(spam) == %g, expected 2", n)
		}

		n = c.getToken(b, "spam", "foo")
		if n != 4 {
			t.Errorf("getToken(spam, foo) == %g, expected 4", n)
		}
		return nil
	})
	if err != nil {
		t.Fatal(err)
	}

}

type trainData struct {
	txt, class string
}

func testClTrain(t *testing.T, c *Classifier, msgs []trainData) {
	for _, m := range msgs {
		comment := &nospam.Comment{
			Comment: m.txt,
			Train:   nospam.Category(m.class),
		}
		words, _ := comment.Data(analyzers.WORDS).(util.StringCounter)

		if err := c.Train(m.class, words); err != nil {
			t.Fatal(err)
		}
	}
}

func testClTest(t *testing.T, c *Classifier, msgs []trainData) {
	for _, m := range msgs {
		comment := &nospam.Comment{
			Comment: m.txt,
			Train:   nospam.Category(m.class),
		}
		words, _ := comment.Data(analyzers.WORDS).(util.StringCounter)

		category := c.Classify(words)
		if category != m.class {
			t.Errorf("message '%s': got %s, expected %s", m.txt, category, m.class)
		}
	}
}

func TestClassifier_Train(t *testing.T) {
	c, _, cleanup := createTestClassifier(t)
	defer cleanup()

	msgs := []trainData{
		{"Buy cialis and viagra", "spam"},
		{"Buy VIAGRA", "spam"},
		{"Today's a good day", "ok"},
	}
	testClTrain(t, c, msgs)

	testClTest(t, c, msgs)
}
