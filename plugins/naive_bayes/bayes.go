package bayes

import (
	"context"
	"errors"

	nospam "git.autistici.org/ai/nospam2"
	"git.autistici.org/ai/nospam2/analyzers"
	"git.autistici.org/ai/nospam2/util"
	"github.com/boltdb/bolt"
)

// Checks the comment body against a Bayesian database.
type bayesPlugin struct {
	classifier *Classifier
}

func (p *bayesPlugin) Name() string { return "bayes" }

func (p *bayesPlugin) Init(_ map[string]interface{}, db *bolt.DB) (err error) {
	p.classifier, err = createClassifier(db, 1.0)
	return
}

func (p *bayesPlugin) Train(ctx context.Context, comment *nospam.Comment) error {
	words, ok := comment.Data(analyzers.WORDS).(util.StringCounter)
	if !ok {
		return errors.New("no 'words' data")
	}

	// Bayes categories are mapped to nospam.Train* values.
	return p.classifier.Train(string(comment.Train), words)
}

func (p *bayesPlugin) Test(ctx context.Context, comment *nospam.Comment) (float64, string, error) {
	words, ok := comment.Data(analyzers.WORDS).(util.StringCounter)
	if !ok {
		return 0, "", errors.New("no 'words' data")
	}

	// Bayes categories are mapped to nospam.Train* values.
	category := p.classifier.Classify(words)
	switch category {
	case nospam.CategorySpam:
		return 1, "classified as spam", nil
	case nospam.CategoryOk:
		return 0, "", nil
	default:
		return 0.5, "maybe spam", nil
	}
}

func init() {
	nospam.RegisterPlugin(&bayesPlugin{})
}
