package bayes

import (
	"encoding/binary"
	"math"
	"sort"

	nospam "git.autistici.org/ai/nospam2"
	"git.autistici.org/ai/nospam2/util"
	"github.com/boltdb/bolt"
)

var Categories = []string{
	nospam.CategoryOk,
	nospam.CategorySpam,
}

type sorted struct {
	category    string
	probability float64
}

// Classifier is what we use to classify documents
type Classifier struct {
	db     *bolt.DB
	bucket []byte

	threshold float64
}

// Create and initialize the classifier
func createClassifier(db *bolt.DB, threshold float64) (*Classifier, error) {
	bucket := []byte("naive_bayes")
	err := db.Update(func(tx *bolt.Tx) error {
		_, err := tx.CreateBucketIfNotExists(bucket)
		if err != nil {
			return err
		}
		return nil
	})
	if err != nil {
		return nil, err
	}
	return &Classifier{
		db:        db,
		bucket:    bucket,
		threshold: threshold,
	}, nil
}

// Increment a key (read-modify-update).
func incrKey(b *bolt.Bucket, key []byte, count int) {
	value := decodeInt(b.Get(key))
	b.Put(key, encodeInt(value+uint64(count))) // nolint: errcheck
}

// Database helpers.
func categoriesDocumentsKey(category string) []byte {
	return appendBytes([]byte("cat_docs/"), []byte(category))
}

func (c *Classifier) incCategoriesDocuments(b *bolt.Bucket, category string) {
	incrKey(b, categoriesDocumentsKey(category), 1)
}

func (c *Classifier) getCategoriesDocuments(b *bolt.Bucket, category string) float64 {
	return float64(decodeInt(b.Get(categoriesDocumentsKey(category))))
}

func categoriesWordsKey(category string) []byte {
	return appendBytes([]byte("cat_words/"), []byte(category))
}

func (c *Classifier) incCategoriesWords(b *bolt.Bucket, category string, count int) {
	incrKey(b, categoriesWordsKey(category), count)
}

func (c *Classifier) getCategoriesWords(b *bolt.Bucket, category string) float64 {
	return float64(decodeInt(b.Get(categoriesWordsKey(category))))
}

func (c *Classifier) incTotalDocuments(b *bolt.Bucket) {
	incrKey(b, []byte("total_docs"), 1)
}

func (c *Classifier) getTotalDocuments(b *bolt.Bucket) float64 {
	return float64(decodeInt(b.Get([]byte("total_docs"))))
}

func (c *Classifier) incTotalTokens(b *bolt.Bucket, count int) {
	incrKey(b, []byte("total_tokens"), count)
}

func tokenKey(category, token string) []byte {
	return appendBytes([]byte("token/"), []byte(category), []byte("/"), []byte(token))
}

func (c *Classifier) incToken(b *bolt.Bucket, category, token string, count int) {
	incrKey(b, tokenKey(category, token), count)
}

func (c *Classifier) getToken(b *bolt.Bucket, category, token string) float64 {
	return float64(decodeInt(b.Get(tokenKey(category, token))))
}

// Train the classifier
func (c *Classifier) Train(category string, words util.StringCounter) error {
	return c.db.Batch(func(tx *bolt.Tx) error {
		b := tx.Bucket(c.bucket)

		// Accumulate all the aggregate increments so that we
		// can call inc() functions just once per key, and we
		// can avoid depending on multiple read/modify/update
		// cycles for the same key.
		var categoriesWordsInc, totalTokensInc int

		for word, count := range words {
			c.incToken(b, category, word, count)
			categoriesWordsInc += count
			totalTokensInc += count
		}

		c.incCategoriesWords(b, category, categoriesWordsInc)
		c.incCategoriesDocuments(b, category)
		//c.incTotalTokens(b, totalTokensInc)
		c.incTotalDocuments(b)
		return nil
	})
}

// Classify a document
func (c *Classifier) Classify(words util.StringCounter) (category string) {
	// get all the probabilities of each category
	prob := c.Probabilities(words)

	// sort the categories according to probabilities
	var sp []sorted
	for c, p := range prob {
		sp = append(sp, sorted{c, p})
	}
	sort.Slice(sp, func(i, j int) bool {
		return sp[i].probability > sp[j].probability
	})

	// Select the highest category above threshold.
	if math.Abs(sp[0].probability-sp[1].probability) > c.threshold {
		category = sp[0].category
	} else {
		category = "unknown"
	}

	// log.Printf(
	// 	"bayes: p(%s)=%g, p(%s)=%g -> %s",
	// 	sp[0].category, sp[0].probability,
	// 	sp[1].category, sp[1].probability,
	// 	category,
	// )

	return
}

// Probabilities of each category
func (c *Classifier) Probabilities(words util.StringCounter) map[string]float64 {
	p := make(map[string]float64)
	// nolint: errcheck
	c.db.View(func(tx *bolt.Tx) error {
		b := tx.Bucket(c.bucket)
		for _, category := range Categories {
			p[category] = c.pCategoryDocument(b, category, words)
		}
		return nil
	})
	return p
}

// p (document | category) [LOG]
func (c *Classifier) logpDocumentCategory(b *bolt.Bucket, category string, words util.StringCounter) (p float64) {
	for word := range words {
		value := c.pWordCategory(b, category, word)
		p += math.Log(value)
	}
	return p
}

func (c *Classifier) pWordCategory(b *bolt.Bucket, category string, word string) float64 {
	// return float64(c.words[category][stem(word)]+1) / float64(c.categoriesWords[category])

	// Laplace smoothing.
	f1 := float64(c.getToken(b, category, word) + 1)
	f2 := float64(c.getCategoriesWords(b, category) + 2)

	return f1 / f2
}

// p (category)
func (c *Classifier) pCategory(b *bolt.Bucket, category string) float64 {
	// return float64(c.categoriesDocuments[category]) / float64(c.totalDocuments)
	f1 := c.getCategoriesDocuments(b, category)
	f2 := c.getTotalDocuments(b)
	return f1 / f2
}

// p (category | document) [LOG]
func (c *Classifier) pCategoryDocument(b *bolt.Bucket, category string, words util.StringCounter) float64 {
	return c.logpDocumentCategory(b, category, words) + math.Log(c.pCategory(b, category))
}

func decodeInt(b []byte) uint64 {
	if b == nil {
		return 0
	}
	return binary.BigEndian.Uint64(b)
}

func encodeInt(i uint64) []byte {
	var b [8]byte
	binary.BigEndian.PutUint64(b[:], i)
	return b[:]
}

func appendBytes(parts ...[]byte) []byte {
	var b []byte
	for _, p := range parts {
		b = append(b, p...)
	}
	return b
}
