package badlink

import (
	"context"
	"regexp"

	nospam "git.autistici.org/ai/nospam2"
	"github.com/boltdb/bolt"
)

// Catch comments with a throwaway bad link (no dot in the domain).
type badlinkPlugin struct{}

var badlinkRx = regexp.MustCompile(`^(https?://)?[a-zA-Z0-9]+$`)

func (p *badlinkPlugin) Name() string { return "badlink" }

func (p *badlinkPlugin) Init(_ map[string]interface{}, _ *bolt.DB) error { return nil }

func (p *badlinkPlugin) Train(ctx context.Context, comment *nospam.Comment) error { return nil }

func (p *badlinkPlugin) Test(ctx context.Context, comment *nospam.Comment) (float64, string, error) {
	if comment.Link != "" && badlinkRx.MatchString(comment.Link) {
		return 1, "bad link", nil
	}
	return 0, "", nil
}

func init() {
	nospam.RegisterPlugin(&badlinkPlugin{})
}
