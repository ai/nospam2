package empty

import (
	"context"
	"regexp"

	nospam "git.autistici.org/ai/nospam2"
	"github.com/boltdb/bolt"
)

// Catch comments with an empty comment body.
type emptyPlugin struct{}

var emptyRx = regexp.MustCompile(`^\s*$`)

func (p *emptyPlugin) Name() string { return "empty" }

func (p *emptyPlugin) Init(_ map[string]interface{}, _ *bolt.DB) error { return nil }

func (p *emptyPlugin) Train(ctx context.Context, comment *nospam.Comment) error { return nil }

func (p *emptyPlugin) Test(ctx context.Context, comment *nospam.Comment) (float64, string, error) {
	if emptyRx.MatchString(comment.Comment) {
		return 1, "empty body", nil
	}
	return 0, "", nil
}

func init() {
	nospam.RegisterPlugin(&emptyPlugin{})
}
