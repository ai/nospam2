package uribl

import (
	"context"
	"errors"
	"fmt"
	"net"
	"strings"

	nospam "git.autistici.org/ai/nospam2"
	"git.autistici.org/ai/nospam2/analyzers"
	"git.autistici.org/ai/nospam2/util"
	"github.com/boltdb/bolt"
	"github.com/miekg/dns"
)

const (
	defaultURIBLDomain = "multi.uribl.com"

	domainDefault   = "default"
	domainBlackA    = "black_a"
	domainBlackNS   = "black_ns"
	domainBlackNSIP = "black_nsip"
)

// Look up comment URLs in the URIBL blocklist.
type uriblPlugin struct {
	domains map[string]string
}

func newURIBLPlugin() *uriblPlugin {
	return &uriblPlugin{
		domains: map[string]string{
			domainDefault: defaultURIBLDomain,
		},
	}
}

func (p *uriblPlugin) Name() string { return "uribl" }

func (p *uriblPlugin) Init(config map[string]interface{}, _ *bolt.DB) error {
	if m, ok := config["uribl_domains"].(map[string]interface{}); ok {
		for k, v := range m {
			if s, ok := v.(string); ok && s != "" {
				p.domains[k] = s
			}
		}
	}

	// if s, ok := config["uribl_whitelist"].(string); ok && s != "" {
	// 	wl, err := util.ReadStringSetFromFile(s)
	// 	if err != nil {
	// 		return err
	// 	}
	// 	p.wl = p.wl.Union(wl)
	// }

	return nil
}

func (p *uriblPlugin) Train(ctx context.Context, comment *nospam.Comment) error { return nil }

func (p *uriblPlugin) Test(ctx context.Context, comment *nospam.Comment) (float64, string, error) {
	// Unify domains and top-level domains into a single dataset.
	domains, ok := comment.Data(analyzers.DOMAINS).(util.StringSet)
	if !ok {
		return 0, "", errors.New("no 'domains' data")
	}
	if tlds, ok := comment.Data(analyzers.TOPLEVEL_DOMAINS).(util.StringSet); ok {
		domains = domains.Union(tlds)
	}
	// if p.wl != nil {
	// 	domains = domains.Difference(p.wl)
	// }

	// Query the DNSBL.
	res := util.NewAsyncResolver()
	for d := range domains {
		// If the domain is a raw IP address, look it up in
		// normalized (reversed) form.
		if ip := net.ParseIP(d); ip != nil {
			// Unless it's IPv6.
			if ip.To4() == nil {
				continue
			}
			d = reverseIP(ip)
		} else {
			// Callback generator that performs an A request and
			// looks up the response in the specified RBL subdomain.
			checkAResponse := func(tag string) func(rr dns.RR) error {
				return func(rr dns.RR) error {
					// This is a generic request so we have to expect CNAMEs.
					switch rrt := rr.(type) {
					case *dns.A:
						if rrt.A.To4() != nil {
							res.Lookup(
								qualify(reverseIP(rrt.A), p.domains[tag]),
								dns.TypeA,
								util.CheckRBLResponse)
						}
					}
					return nil
				}
			}

			// Query URIBL for the A record.
			if p.hasDomain(domainBlackA) {
				res.Lookup(d, dns.TypeA, checkAResponse(domainBlackA))
			}
			// Query URIBL for the NS domain, and for the
			// A record of the NS itself.
			if p.hasDomain(domainBlackNS) {
				res.Lookup(d, dns.TypeNS, func(rr dns.RR) error {
					switch rrt := rr.(type) {
					case *dns.NS:
						res.Lookup(
							qualify(strings.TrimRight(rrt.Ns, "."), p.domains[domainBlackNS]),
							dns.TypeA,
							util.CheckRBLResponse)
						if p.hasDomain(domainBlackNSIP) {
							res.Lookup(
								dns.Fqdn(rrt.Ns),
								dns.TypeA,
								checkAResponse(domainBlackNSIP))
						}
					}
					return nil
				})
			}
		}

		res.Lookup(qualify(d, p.domains[domainDefault]), dns.TypeA, util.CheckRBLResponse)
	}

	err := res.Wait(ctx)
	switch err {
	case util.ErrAdminBlock:
		return 0, "", err
	case util.ErrRBLMatch:
		return 1, "URL found in URIBL", nil
	default:
		return 0, "", nil
	}
}

func (p *uriblPlugin) hasDomain(tag string) bool {
	_, ok := p.domains[tag]
	return ok
}

func qualify(d, domain string) string {
	return fmt.Sprintf("%s.%s.", d, domain)
}

func reverseIP(ip net.IP) string {
	revip := net.IPv4(ip[3], ip[2], ip[1], ip[0])
	return revip.String()
}

func init() {
	nospam.RegisterPlugin(newURIBLPlugin())
}
