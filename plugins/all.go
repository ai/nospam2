package plugins

import (
	_ "git.autistici.org/ai/nospam2/plugins/badlink"
	_ "git.autistici.org/ai/nospam2/plugins/classified"
	_ "git.autistici.org/ai/nospam2/plugins/empty"
	_ "git.autistici.org/ai/nospam2/plugins/freemail"
	_ "git.autistici.org/ai/nospam2/plugins/goodlinks"
	_ "git.autistici.org/ai/nospam2/plugins/linkabuse"
	_ "git.autistici.org/ai/nospam2/plugins/linkcount"
	_ "git.autistici.org/ai/nospam2/plugins/naive_bayes"
	_ "git.autistici.org/ai/nospam2/plugins/nameisurl"
	_ "git.autistici.org/ai/nospam2/plugins/nofollow"
	_ "git.autistici.org/ai/nospam2/plugins/stopwords"
	_ "git.autistici.org/ai/nospam2/plugins/strong"
	_ "git.autistici.org/ai/nospam2/plugins/surbl"
	_ "git.autistici.org/ai/nospam2/plugins/weirdurls"
)
