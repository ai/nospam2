package stopwords

import (
	"context"
	"errors"
	"fmt"

	nospam "git.autistici.org/ai/nospam2"
	"git.autistici.org/ai/nospam2/analyzers"
	"git.autistici.org/ai/nospam2/util"
	"github.com/agnivade/levenshtein"
	"github.com/boltdb/bolt"
)

// Check for the presence of known 100%-spammy keywords, accounting
// for possible mis-spellings.
type stopwordsPlugin struct {
	words util.StringSet
}

func newStopwordsPlugin() *stopwordsPlugin {
	return &stopwordsPlugin{
		words: util.NewStringSetFromList([]string{"viagra", "cialis"}),
	}
}

func (p *stopwordsPlugin) Name() string { return "stopwords" }

func (p *stopwordsPlugin) Init(config map[string]interface{}, _ *bolt.DB) error {
	if s, ok := config["stopwords"].(string); ok && s != "" {
		words, err := util.ReadStringSetFromFile(s)
		if err != nil {
			return err
		}
		p.words = words
	}
	return nil
}

func (p *stopwordsPlugin) Train(ctx context.Context, comment *nospam.Comment) error { return nil }

func (p *stopwordsPlugin) Test(ctx context.Context, comment *nospam.Comment) (float64, string, error) {
	words, ok := comment.Data(analyzers.WORDS).(util.StringCounter)
	if !ok {
		return 0, "", errors.New("no 'words' data")
	}

	for stopword := range p.words {
		for w := range words {
			if areSimilar(stopword, w) {
				return 1, fmt.Sprintf("stopword '%s'", w), nil
			}
		}
	}
	return 0, "", nil
}

func areSimilar(w1, w2 string) bool {
	if w1 == w2 {
		return true
	}
	// Allow 1 edit every 4 characters, starting with strings of
	// at least length 4.
	maxDistance := (1 + len(w1)) / 4
	dist := levenshtein.ComputeDistance(w1, w2)
	return dist <= maxDistance
}

func init() {
	nospam.RegisterPlugin(newStopwordsPlugin())
}
