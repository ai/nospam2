package goodlinks

import (
	"context"
	"errors"
	"fmt"

	nospam "git.autistici.org/ai/nospam2"
	"git.autistici.org/ai/nospam2/analyzers"
	"git.autistici.org/ai/nospam2/util"
	"github.com/boltdb/bolt"
)

var defaultGoodLinks = []string{
	"autistici.org",
	"inventati.org",
	"noblogs.org",
	"anche.no",
	"paranoici.org",
	"insiberia.net",
	"riseup.net",
}

const BIAS = -1.0

// Bias positively (using a negative score) comments that contain
// links to well-known domains.
type goodlinksPlugin struct {
	domains util.StringSet
}

func newGoodlinksPlugin() *goodlinksPlugin {
	return &goodlinksPlugin{
		domains: util.NewStringSetFromList(defaultGoodLinks),
	}
}

func (p *goodlinksPlugin) Name() string { return "goodlinks" }

func (p *goodlinksPlugin) Init(config map[string]interface{}, _ *bolt.DB) error {
	if s, ok := config["goodlinks_file"].(string); ok && s != "" {
		domains, err := util.ReadStringSetFromFile(s)
		if err != nil {
			return err
		}
		p.domains = domains
	}
	return nil
}

func (p *goodlinksPlugin) Train(ctx context.Context, comment *nospam.Comment) error { return nil }

func (p *goodlinksPlugin) Test(ctx context.Context, comment *nospam.Comment) (float64, string, error) {
	tlds, ok := comment.Data(analyzers.TOPLEVEL_DOMAINS).(util.StringSet)
	if !ok {
		return 0, "", errors.New("no 'toplevel_domains' data")
	}

	emailDomains, ok := comment.Data(analyzers.EMAIL_DOMAINS).(util.StringSet)
	if !ok {
		return 0, "", errors.New("no 'email_domains' data'")
	}

	for _, m := range []util.StringSet{tlds, emailDomains} {
		for domain := range m {
			if _, ok := p.domains.HasPrefix(domain); ok {
				return BIAS, fmt.Sprintf("good link %s", domain), nil
			}
		}
	}

	return 0, "", nil
}

func init() {
	nospam.RegisterPlugin(newGoodlinksPlugin())
}
