package nofollow

import (
	"context"
	"regexp"

	nospam "git.autistici.org/ai/nospam2"
	"github.com/boltdb/bolt"
)

type nofollowPlugin struct{}

var rxs = []*regexp.Regexp{
	regexp.MustCompile(`<a href=["'][^#"][^"']+["']\s+[^>]*rel=["']?nofollow`),
	regexp.MustCompile(`<a\s+rel=["']?nofollow[^>]+href=`),
}

func (p *nofollowPlugin) Name() string { return "nofollow" }

func (p *nofollowPlugin) Init(_ map[string]interface{}, _ *bolt.DB) error { return nil }

func (p *nofollowPlugin) Train(ctx context.Context, comment *nospam.Comment) error { return nil }

func (p *nofollowPlugin) Test(ctx context.Context, comment *nospam.Comment) (float64, string, error) {
	if comment.BodyMatches(rxs) {
		return 1, "detected nofollow link", nil
	}
	return 0, "", nil
}

func init() {
	nospam.RegisterPlugin(&nofollowPlugin{})
}
