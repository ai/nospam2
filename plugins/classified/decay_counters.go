package classified

import (
	"encoding/binary"
	"math"
	"time"

	"github.com/boltdb/bolt"
)

type DecayCounterMap struct {
	db     *bolt.DB
	bucket []byte

	Tau float64
}

func NewDecayCounterMap(db *bolt.DB, name string) (*DecayCounterMap, error) {
	bucket := []byte(name)
	err := db.Update(func(tx *bolt.Tx) error {
		_, err := tx.CreateBucketIfNotExists(bucket)
		if err != nil {
			return err
		}
		return nil
	})
	if err != nil {
		return nil, err
	}
	return &DecayCounterMap{
		db:     db,
		bucket: bucket,
		Tau:    7.0,
	}, nil
}

func (m *DecayCounterMap) Get(key string) (uint64, error) {
	var value uint64
	err := m.db.View(func(tx *bolt.Tx) error {
		b := tx.Bucket(m.bucket)
		value = m.getInterpolatedValue(b, key)
		return nil
	})
	return value, err
}

func (m *DecayCounterMap) Inc(key string) error {
	return m.db.Update(func(tx *bolt.Tx) error {
		b := tx.Bucket(m.bucket)
		curCount := m.getInterpolatedValue(b, key)
		return m.setValueAndTimestamp(b, key, curCount+1, time.Now())
	})
}

func (m *DecayCounterMap) getInterpolatedValue(b *bolt.Bucket, key string) uint64 {
	value, ts := m.getValueAndTimestamp(b, key)
	days := time.Since(ts).Seconds() / 86400.0
	value = uint64(float64(value) * math.Exp(-days/m.Tau))
	return value
}

func (m *DecayCounterMap) getValueAndTimestamp(b *bolt.Bucket, key string) (value uint64, stamp time.Time) {
	if data := b.Get(mkKey(key)); data != nil {
		value = binary.BigEndian.Uint64(data[:8])
		unixStamp := binary.BigEndian.Uint64(data[8:])
		stamp = time.Unix(int64(unixStamp), 0)
	}
	return
}

func (m *DecayCounterMap) setValueAndTimestamp(b *bolt.Bucket, key string, value uint64, stamp time.Time) error {
	var enc [16]byte
	binary.BigEndian.PutUint64(enc[:8], value)
	binary.BigEndian.PutUint64(enc[8:], uint64(stamp.Unix()))
	return b.Put(mkKey(key), enc[:])
}

func mkKey(key string) []byte {
	// No prefix needed, we have our own bucket.
	return []byte(key)
}

func valueKey(key string) []byte {
	return []byte("v/" + key)
}

func timestampKey(key string) []byte {
	return []byte("ts/" + key)
}
