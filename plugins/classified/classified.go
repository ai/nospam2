package classified

import (
	"context"
	"errors"
	"fmt"
	"log"

	nospam "git.autistici.org/ai/nospam2"
	"git.autistici.org/ai/nospam2/analyzers"
	"git.autistici.org/ai/nospam2/util"
	"github.com/boltdb/bolt"
)

// Count links to the same domains and adds them to an
// exponentially-decaying block list when we see too many hits.
type classifiedPlugin struct {
	counters *DecayCounterMap
	limit    uint64
	allow    util.StringSet
}

func newClassifiedPlugin() *classifiedPlugin {
	return &classifiedPlugin{
		limit: 20,
	}
}

func (p *classifiedPlugin) Name() string { return "classified" }

func (p *classifiedPlugin) Init(config map[string]interface{}, db *bolt.DB) error {
	counters, err := NewDecayCounterMap(db, "classified")
	if err != nil {
		return err
	}
	p.counters = counters

	if i, ok := config["classified_limit"].(int); ok && i > 0 {
		p.limit = uint64(i)
	}

	if f, ok := config["classified_allow_list"].(string); ok && f != "" {
		wl, err := util.ReadStringSetFromFile(f)
		if err != nil {
			return err
		}
		p.allow = wl
	}

	return nil
}

func (p *classifiedPlugin) Test(ctx context.Context, comment *nospam.Comment) (float64, string, error) {
	tlds, ok := comment.Data(analyzers.TOPLEVEL_DOMAINS).(util.StringSet)
	if !ok {
		return 0, "", errors.New("no 'toplevel_domains' data")
	}

	for domain := range tlds {
		if p.allow != nil {
			if _, ok := p.allow.HasPrefix(domain); ok {
				continue
			}
		}
		value, err := p.counters.Get(domain)
		if err != nil {
			continue
		}
		if value > p.limit {
			return 1, fmt.Sprintf("more than %d links from domain %s", p.limit, domain), nil
		}
	}

	return 0, "", nil
}

func (p *classifiedPlugin) Train(ctx context.Context, comment *nospam.Comment) error {
	// Only train on spam messages.
	if comment.Train != "spam" {
		return nil
	}

	tlds, ok := comment.Data(analyzers.TOPLEVEL_DOMAINS).(util.StringSet)
	if !ok {
		return nil
	}

	for domain := range tlds {
		if err := p.counters.Inc(domain); err != nil {
			log.Printf("error incrementing counter for %s: %v", domain, err)
		}
	}

	return nil
}

func init() {
	nospam.RegisterPlugin(newClassifiedPlugin())
}
