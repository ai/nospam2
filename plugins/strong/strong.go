package strong

import (
	"context"
	"regexp"

	nospam "git.autistici.org/ai/nospam2"
	"github.com/boltdb/bolt"
)

// Tests for messages that begin with a <strong> tag. This is a very
// old style of pingback comment, frequently abused by spammers.
type strongPlugin struct{}

var rxs = []*regexp.Regexp{
	regexp.MustCompile(`^\s*<(strong|STRONG)`),
}

func (p *strongPlugin) Name() string { return "strong" }

func (p *strongPlugin) Init(_ map[string]interface{}, _ *bolt.DB) error { return nil }

func (p *strongPlugin) Train(ctx context.Context, comment *nospam.Comment) error { return nil }

func (p *strongPlugin) Test(ctx context.Context, comment *nospam.Comment) (float64, string, error) {
	if comment.BodyMatches(rxs) {
		return 1, "<strong>", nil
	}
	return 0, "", nil
}

func init() {
	nospam.RegisterPlugin(&strongPlugin{})
}
