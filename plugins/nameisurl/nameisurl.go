package nameisurl

import (
	"context"

	nospam "git.autistici.org/ai/nospam2"
	"github.com/boltdb/bolt"
	"mvdan.cc/xurls/v2"
)

// Catch comments that have a URL in the "author name" field.
type nameisurlPlugin struct{}

var urlRx = xurls.Relaxed()

func (p *nameisurlPlugin) Name() string { return "nameisurl" }

func (p *nameisurlPlugin) Init(_ map[string]interface{}, _ *bolt.DB) error { return nil }

func (p *nameisurlPlugin) Train(ctx context.Context, comment *nospam.Comment) error { return nil }

func (p *nameisurlPlugin) Test(ctx context.Context, comment *nospam.Comment) (float64, string, error) {
	if urlRx.MatchString(comment.Author) {
		return 1, "name contains URL", nil
	}
	return 0, "", nil
}

func init() {
	nospam.RegisterPlugin(&nameisurlPlugin{})
}
