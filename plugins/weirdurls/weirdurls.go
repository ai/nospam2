package weirdurls

import (
	"context"
	"regexp"

	nospam "git.autistici.org/ai/nospam2"
	"github.com/boltdb/bolt"
)

// Checks for the usage of [URL=]...[/URL] markup constructs, which
// are not used by our blog software.
type weirdurlsPlugin struct{}

var rxs = []*regexp.Regexp{
	regexp.MustCompile(`\[(url|URL)=.*\[/(url|URL)]`),
}

func (p *weirdurlsPlugin) Name() string { return "weirdurls" }

func (p *weirdurlsPlugin) Init(_ map[string]interface{}, _ *bolt.DB) error { return nil }

func (p *weirdurlsPlugin) Train(ctx context.Context, comment *nospam.Comment) error { return nil }

func (p *weirdurlsPlugin) Test(ctx context.Context, comment *nospam.Comment) (float64, string, error) {
	if comment.BodyMatches(rxs) {
		return 1, "detected weird URL link", nil
	}
	return 0, "", nil
}

func init() {
	nospam.RegisterPlugin(&weirdurlsPlugin{})
}
