package linkcount

import (
	"context"
	"fmt"
	"strconv"

	nospam "git.autistici.org/ai/nospam2"
	"git.autistici.org/ai/nospam2/analyzers"
	"github.com/boltdb/bolt"
)

// Check if the comment has too many links.
type linkCountPlugin struct {
	maxLinks int
}

func newLinkCountPlugin() *linkCountPlugin {
	return &linkCountPlugin{
		maxLinks: 3,
	}
}

func (p *linkCountPlugin) Name() string { return "linkcount" }

func (p *linkCountPlugin) Init(config map[string]interface{}, _ *bolt.DB) error {
	s, ok := config["max_links"].(string)
	if !ok {
		return nil
	}
	n, err := strconv.Atoi(s)
	if err != nil {
		return fmt.Errorf("max_links: %w", err)
	}
	p.maxLinks = n
	return nil
}

func (p *linkCountPlugin) Train(ctx context.Context, comment *nospam.Comment) error { return nil }

func (p *linkCountPlugin) Test(ctx context.Context, comment *nospam.Comment) (float64, string, error) {
	if urls, ok := comment.Data(analyzers.URLS).(*analyzers.URLInfo); ok {
		if urls.URLCount > p.maxLinks {
			return 1, fmt.Sprintf("%d links found", urls.URLCount), nil
		}
	}
	return 0, "", nil
}

func init() {
	nospam.RegisterPlugin(newLinkCountPlugin())
}
