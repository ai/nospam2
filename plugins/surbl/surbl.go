package surbl

import (
	"context"
	"errors"
	"fmt"
	"net"

	nospam "git.autistici.org/ai/nospam2"
	"git.autistici.org/ai/nospam2/analyzers"
	"git.autistici.org/ai/nospam2/util"
	"github.com/boltdb/bolt"
	"github.com/miekg/dns"
)

const defaultSURBLDomain = "multi.surbl.org"

// Look up comment URLs in the SURBL blocklist.
type surblPlugin struct {
	domain string
	wl     util.StringSet
}

func newSURBLPlugin() *surblPlugin {
	return &surblPlugin{
		domain: defaultSURBLDomain,
		wl:     util.NewStringSetFromList(surblAllowList),
	}
}

func (p *surblPlugin) Name() string { return "surbl" }

func (p *surblPlugin) Init(config map[string]interface{}, _ *bolt.DB) error {
	if s, ok := config["surbl_domain"].(string); ok && s != "" {
		p.domain = s
	}

	if s, ok := config["surbl_whitelist"].(string); ok && s != "" {
		wl, err := util.ReadStringSetFromFile(s)
		if err != nil {
			return err
		}
		p.wl = p.wl.Union(wl)
	}

	return nil
}

func (p *surblPlugin) Train(ctx context.Context, comment *nospam.Comment) error { return nil }

func (p *surblPlugin) Test(ctx context.Context, comment *nospam.Comment) (float64, string, error) {
	// Unify domains and top-level domains into a single dataset.
	domains, ok := comment.Data(analyzers.DOMAINS).(util.StringSet)
	if !ok {
		return 0, "", errors.New("no 'domains' data")
	}
	if tlds, ok := comment.Data(analyzers.TOPLEVEL_DOMAINS).(util.StringSet); ok {
		domains = domains.Union(tlds)
	}
	if p.wl != nil {
		domains = domains.Difference(p.wl)
	}

	// Query the DNSBL.
	res := util.NewAsyncResolver()
	for d := range domains {
		name, ok := surblName(d, p.domain)
		if !ok {
			continue
		}
		res.Lookup(name, dns.TypeA, util.CheckRBLResponse)
	}

	err := res.Wait(ctx)
	switch err {
	case util.ErrAdminBlock:
		return 0, "", err
	case util.ErrRBLMatch:
		return 1, "URL found in SURBL", nil
	default:
		return 0, "", nil
	}
}

func surblName(s, surblDomain string) (string, bool) {
	// Is it an IP?
	if ip := net.ParseIP(s); ip != nil {
		// Reverse IPv4.
		if ip4 := ip.To4(); ip4 != nil {
			revip := net.IPv4(ip4[3], ip4[2], ip4[1], ip4[0])
			return fmt.Sprintf("%s.%s", revip.String(), surblDomain), true
		}
		// Drop IPv6.
		return "", false
	}

	// Just append the SURBL domain.
	return fmt.Sprintf("%s.%s", s, surblDomain), true
}

func init() {
	// TODO: Remove completely. Should be replaced by URIBL.
	//nospam.RegisterPlugin(newSURBLPlugin())
}
