package freemail

import (
	"context"
	"errors"
	"fmt"
	"regexp"
	"strings"

	nospam "git.autistici.org/ai/nospam2"
	"git.autistici.org/ai/nospam2/analyzers"
	"git.autistici.org/ai/nospam2/util"
	"github.com/boltdb/bolt"
)

// See if the comment author is using a freemail account (according to
// the Spamassassin definition).
type freemailPlugin struct{}

var (
	freemailDomainsRx []*regexp.Regexp
)

func (p *freemailPlugin) Name() string { return "freemail" }

func (p *freemailPlugin) Init(_ map[string]interface{}, _ *bolt.DB) error { return nil }

func (p *freemailPlugin) Train(ctx context.Context, comment *nospam.Comment) error { return nil }

func (p *freemailPlugin) Test(ctx context.Context, comment *nospam.Comment) (float64, string, error) {
	domains, ok := comment.Data(analyzers.EMAIL_DOMAINS).(util.StringSet)
	if !ok {
		return 0, "", errors.New("no 'email_domains' data")
	}

	for domain := range domains {
		for _, rx := range freemailDomainsRx {
			if rx.MatchString(domain) {
				return 1, "freemail address", nil
			}
		}
	}

	return 0, "", nil
}

func init() {
	nospam.RegisterPlugin(&freemailPlugin{})

	freemailDomainsRx = make([]*regexp.Regexp, 0, len(freemailDomainsList))
	for _, d := range freemailDomainsList {
		pattern := fmt.Sprintf(
			"(^|\\.)%s$",
			strings.Replace(
				strings.Replace(
					d, ".", "\\.", -1,
				),
				"*", ".*", -1,
			),
		)
		rx := regexp.MustCompile(pattern)
		freemailDomainsRx = append(freemailDomainsRx, rx)
	}
}
