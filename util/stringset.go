package util

import (
	"fmt"
	"strings"
)

type StringSet map[string]struct{}

func NewStringSet() StringSet {
	return make(map[string]struct{})
}

func NewStringSetFromList(l []string) StringSet {
	s := NewStringSet()
	for _, elem := range l {
		s.Add(elem)
	}
	return s
}

func (s StringSet) String() string {
	tmp := make([]string, 0, len(s))
	for k := range s {
		tmp = append(tmp, k)
	}
	return fmt.Sprintf("{%s}", strings.Join(tmp, ", "))
}

func (s StringSet) Has(key string) bool {
	_, ok := s[key]
	return ok
}

func (s StringSet) HasPrefix(key string) (string, bool) {
	n := len(key)
	for n > 0 {
		n = strings.LastIndexByte(key[:n], '.')
		cur := key[n+1:]
		if _, ok := s[cur]; ok {
			return cur, true
		}
	}
	return "", false
}

func (s StringSet) Add(key string) {
	s[key] = struct{}{}
}

func (s StringSet) Union(other StringSet) StringSet {
	union := NewStringSet()
	for k := range s {
		union.Add(k)
	}
	for k := range other {
		union.Add(k)
	}
	return union
}

func (s StringSet) Difference(other StringSet) StringSet {
	diff := NewStringSet()
	for a := range s {
		if !other.Has(a) {
			diff.Add(a)
		}
	}
	return diff
}

type StringCounter map[string]int

func NewStringCounter() StringCounter {
	return make(map[string]int)
}

func (c StringCounter) Inc(key string) {
	c[key]++
}
