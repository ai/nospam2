package util

import "testing"

type testData struct {
	key      string
	expected bool
}

type testPrefixData struct {
	key            string
	expected       bool
	expectedPrefix string
}

func runTestHas(t *testing.T, set StringSet, td []testData) {
	for _, elem := range td {
		ok := set.Has(elem.key)
		if ok != elem.expected {
			t.Errorf("Has(%s) -> %v, expected %v", elem.key, ok, elem.expected)
		}
	}
}

func runTestHasPrefix(t *testing.T, set StringSet, td []testPrefixData) {
	for _, elem := range td {
		pfx, ok := set.HasPrefix(elem.key)
		if ok != elem.expected {
			t.Errorf("HasPrefix(%s) -> %v, expected %v", elem.key, ok, elem.expected)
		}
		if ok && pfx != elem.expectedPrefix {
			t.Errorf("HasPrefix(%s) -> tld=%s, expected %s", elem.key, pfx, elem.expectedPrefix)
		}
	}
}

func TestStringSet_Has(t *testing.T) {
	set := NewStringSetFromList([]string{"foo.com", "bar.org"})
	runTestHas(t, set, []testData{
		{"foo.com", true},
		{"bar.org", true},
		{"bar.xyz", false},
		{"www.foo.com", false},
		{"one.two.three.foo.com", false},
	})
	runTestHasPrefix(t, set, []testPrefixData{
		{"foo.com", true, "foo.com"},
		{"bar.org", true, "bar.org"},
		{"bar.xyz", false, ""},
		{"www.foo.com", true, "foo.com"},
		{"one.two.three.foo.com", true, "foo.com"},
	})
}
