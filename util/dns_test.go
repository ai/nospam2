package util

import (
	"context"
	"fmt"
	"testing"
	"time"

	"github.com/miekg/dns"
)

func TestAsyncResolver(t *testing.T) {
	res := NewAsyncResolver()

	N := 30
	for i := 0; i < N; i++ {
		res.Lookup(fmt.Sprintf("%d.baidu.com", i), dns.TypeA, func(rr dns.RR) error {
			fmt.Println(rr)
			return nil
		})
	}

	ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
	defer cancel()

	if err := res.Wait(ctx); err != nil {
		t.Fatal(err)
	}
}
