package util

import (
	"context"
	"errors"
	"math/rand"
	"net"
	"time"

	"github.com/miekg/dns"
)

var (
	dnsClient   = new(dns.Client)
	nameservers = []string{"8.8.8.8", "8.8.4.4"}
	dnsTimeout  = 3 * time.Second
)

func init() {
	if resolvConf, err := dns.ClientConfigFromFile("/etc/resolv.conf"); err == nil && len(resolvConf.Servers) > 0 {
		var ns []string
		for _, s := range resolvConf.Servers {
			ns = append(ns, net.JoinHostPort(s, resolvConf.Port))
		}
		nameservers = ns
	}
}

func getNameserver() string {
	return nameservers[rand.Intn(len(nameservers))]
}

type dnsLookupRequest struct {
	Name  string
	Qtype uint16
}

func (r *dnsLookupRequest) send(ctx context.Context, conn *dns.Conn) error {
	msg := new(dns.Msg)
	msg.SetQuestion(r.Name, r.Qtype)
	applyTimeoutsFromContext(ctx, conn, dnsTimeout)
	return conn.WriteMsg(msg)
}

type DNSLookupCallbackFunc func(dns.RR) error

type dnsLookupMultiRequest map[dnsLookupRequest]DNSLookupCallbackFunc

// AsyncResolver is a callback-based async DNS resolver, which can be
// simpler in certain cases than spawning a goroutine for each lookup
// and having to handle the concurrency explicitly.
//
// The resolver is not goroutine-safe, but it is fully re-entrant,
// i.e. you can safely call Lookup() from a result callback.
//
type AsyncResolver struct {
	pending dnsLookupMultiRequest

	// These exist so that we can call Lookup() from a callback,
	// and magically refer to the same connection and controlling
	// context without having to pass them around.
	conn *dns.Conn
	ctx  context.Context
}

func NewAsyncResolver() *AsyncResolver {
	return &AsyncResolver{
		pending: make(dnsLookupMultiRequest),
	}
}

// Lookup a DNS record and execute the specified callback if we get a
// successful result. Note that only a single callback will be called
// for each name / qtype combination.
func (r *AsyncResolver) Lookup(name string, qtype uint16, fn DNSLookupCallbackFunc) {
	req := dnsLookupRequest{Name: dns.Fqdn(name), Qtype: qtype}
	r.pending[req] = fn
	if r.conn != nil {
		req.send(r.ctx, r.conn)
	}
}

// Wait until all requests are done. This function actually hides the
// entire run loop of the resolver, making requests and invoking
// callbacks when responses are received.
//
// If a callback returns a non-nil error, the resolver will immediately
// abort further requests and return.
//
func (r *AsyncResolver) Wait(ctx context.Context) error {
	conn, err := dnsClient.DialContext(ctx, getNameserver())
	if err != nil {
		return err
	}
	r.conn = conn
	r.ctx = ctx
	defer func() {
		conn.Close()
		r.conn = nil
		r.ctx = nil
	}()

	for req := range r.pending {
		req.send(ctx, conn)
	}

	for len(r.pending) > 0 {
		applyTimeoutsFromContext(ctx, conn, dnsTimeout)
		msg, err := conn.ReadMsg()
		if err != nil {
			return err
		}
		if len(msg.Question) < 1 {
			continue
		}
		// Find the corresponding request, and call the associated callback.
		req := dnsLookupRequest{Name: msg.Question[0].Name, Qtype: msg.Question[0].Qtype}
		if fn, ok := r.pending[req]; ok {
			delete(r.pending, req)

			for _, rr := range msg.Answer {
				if err := fn(rr); err != nil {
					return err
				}
			}
		}
	}

	return nil
}

func applyTimeoutsFromContext(ctx context.Context, conn net.Conn, maxTimeout time.Duration) {
	t := time.Now().Add(maxTimeout)
	if deadline, ok := ctx.Deadline(); ok {
		if deadline.Before(t) {
			t = deadline
		}
	}
	conn.SetWriteDeadline(t)
	conn.SetReadDeadline(t)
}

// Errors returned by CheckRBLResponse.
var (
	// ErrAdminBlock is returned when our RBL lookups are being blocked.
	ErrAdminBlock = errors.New("ADMIN_BLOCK")

	// ErrRBLMatch is returned for all other RBL matches.
	ErrRBLMatch = errors.New("match")
)

// CheckRBLResponse is a callback that will test the response from a
// RBL and return an appropriate error.
func CheckRBLResponse(rr dns.RR) error {
	lsb := rr.(*dns.A).A[3]

	if lsb&1 == 1 {
		// Administrative block.
		return ErrAdminBlock
	}

	return ErrRBLMatch
}
