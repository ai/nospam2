package util

import (
	"context"
	"io"
	"log"
	"net/http"
	"sync"
	"time"

	"golang.org/x/net/html/charset"
)

type RemoteStringSet struct {
	uri    string
	client *http.Client

	mx      sync.RWMutex
	current StringSet
}

func NewRemoteStringSet(uri string, dflt StringSet, updateInterval time.Duration) *RemoteStringSet {
	if dflt == nil {
		dflt = NewStringSet()
	}
	s := &RemoteStringSet{
		uri:     uri,
		client:  new(http.Client),
		current: dflt,
	}
	go s.updateLoop(updateInterval)
	return s
}

func (s *RemoteStringSet) update(ctx context.Context) error {
	req, err := http.NewRequest("GET", s.uri, nil)
	if err != nil {
		return err
	}
	resp, err := s.client.Do(req.WithContext(ctx))
	if err != nil {
		return err
	}
	defer resp.Body.Close()

	var r io.Reader
	r = resp.Body
	if ct := resp.Header.Get("Content-Type"); ct != "" {
		r, err = charset.NewReader(r, ct)
		if err != nil {
			return err
		}
	}

	set, err := ReadStringSet(r)
	if err != nil {
		return err
	}

	s.mx.Lock()
	s.current = set
	s.mx.Unlock()
	return nil
}

func (s *RemoteStringSet) Set() StringSet {
	s.mx.RLock()
	defer s.mx.RUnlock()
	return s.current
}

func (s *RemoteStringSet) updateLoop(updateInterval time.Duration) {
	tick := time.NewTicker(updateInterval)
	for {
		ctx, cancel := context.WithTimeout(context.Background(), 60*time.Second)
		if err := s.update(ctx); err != nil {
			log.Printf("error updating remote data source %s: %v", s.uri, err)
		}
		cancel()

		<-tick.C
	}
}
