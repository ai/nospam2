package util

import (
	"bufio"
	"io"
	"os"
	"strings"
)

func ReadStringSet(r io.Reader) (StringSet, error) {
	set := NewStringSet()
	scan := bufio.NewScanner(r)
	for scan.Scan() {
		s := strings.TrimSpace(scan.Text())
		if s == "" || strings.HasPrefix(s, "#") {
			continue
		}
		set.Add(s)
	}
	return set, scan.Err()
}

func ReadStringSetFromFile(path string) (StringSet, error) {
	f, err := os.Open(path)
	if err != nil {
		return nil, err
	}
	defer f.Close()
	return ReadStringSet(f)
}
