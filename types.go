package nospam

import (
	"encoding/json"
	"log"
	"regexp"
	"sync"
)

// Comment represents the fundamental unit of processing in
// nospam. Comment fields are modeled after the legacy Blogspam API.
//
// Besides being part of the public nospam API, Comment objects
// internally support lazy computation of derived datasets by using
// named Analyzers. These datasets are used by multiple plugins, but
// only computed once.
type Comment struct {
	Agent   string `json:"agent"`
	Author  string `json:"name"`
	Comment string `json:"comment"`
	Email   string `json:"email"`
	Link    string `json:"link"`
	Site    string `json:"site"`

	// Only used when training.
	Train Category `json:"train,omitempty"`

	// Internal
	dataMx sync.Mutex
	data   map[string]*datum
}

// String serialization, for debugging purposes (just a JSON dump).
func (c *Comment) String() string {
	data, _ := json.Marshal(c)
	return string(data)
}

// Analyzer provides additional data derived from a Comment.
type Analyzer interface {
	Analyze(*Comment) interface{}
}

var analyzers = make(map[string]Analyzer)

func RegisterAnalyzer(key string, a Analyzer) {
	analyzers[key] = a
}

type datum struct {
	data interface{}
	once sync.Once
}

// Data returns the results of running the named Analyzer on this
// comment. The caller is expected to know the type of the result.
func (c *Comment) Data(key string) interface{} {
	a, ok := analyzers[key]
	if !ok {
		log.Printf("unknown analyzer '%s' requested!", key)
		return nil
	}

	c.dataMx.Lock()
	if c.data == nil {
		c.data = make(map[string]*datum)
	}
	d, ok := c.data[key]
	if !ok {
		d = new(datum)
		c.data[key] = d
	}
	c.dataMx.Unlock()

	d.once.Do(func() {
		d.data = a.Analyze(c)
	})
	return d.data
}

// BodyMatches checks if the comment body ("comment" field) matches
// against any of the regular expressions provided.
func (c *Comment) BodyMatches(rxList []*regexp.Regexp) bool {
	for _, rx := range rxList {
		if rx.MatchString(c.Comment) {
			return true
		}
	}
	return false
}

// Status is the outcome of an individual test performed by a plugin.
type Status string

// Possible result statuses for an individual test.
const (
	StatusOk    = "ok"
	StatusError = "error"
)

// Category for message classification / training.
type Category string

// Known categories for training and classification.
const (
	CategoryOk   = "ok"
	CategorySpam = "spam"
)

// TestResult is the result of an individual test (plugin).
type TestResult struct {
	Name    string  `json:"name"`
	Score   float64 `json:"score"`
	Status  Status  `json:"status"`
	Message string  `json:"message"`
}

// Result of the classification of a Comment.
type Result struct {
	Category Category      `json:"category"`
	Score    float64       `json:"score"`
	Tests    []*TestResult `json:"tests"`
}

// String serialization of a result, for debugging purposes.
func (r *Result) String() string {
	data, _ := json.Marshal(r)
	return string(data)
}
