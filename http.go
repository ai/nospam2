package nospam

import (
	"encoding/json"
	"log"
	"net/http"
)

func (s *Engine) ServeHTTP(w http.ResponseWriter, req *http.Request) {
	switch {
	case req.URL.Path == "/api/test" && req.Method == "POST":
		s.handleHTTPTest(w, req)
	case req.URL.Path == "/api/classify" && req.Method == "POST":
		s.handleHTTPClassify(w, req)
	default:
		http.NotFound(w, req)
	}
}

func (s *Engine) handleHTTPTest(w http.ResponseWriter, req *http.Request) {
	var comment Comment
	if err := json.NewDecoder(req.Body).Decode(&comment); err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}

	result, err := s.Test(req.Context(), &comment)
	if err != nil {
		log.Printf("test error: %v", err)
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	log.Printf("test site=%s result=%s", niceSite(comment.Site), result.String())

	w.Header().Set("Content-Type", "application/json; charset=utf-8")
	w.Header().Set("Cache-Control", "no-store")
	json.NewEncoder(w).Encode(result)
}

func (s *Engine) handleHTTPClassify(w http.ResponseWriter, req *http.Request) {
	var comment Comment
	if err := json.NewDecoder(req.Body).Decode(&comment); err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}

	err := s.Train(req.Context(), &comment)
	if err != nil {
		log.Printf("classify error: %v", err)
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	log.Printf("classify site=%s category=%s", niceSite(comment.Site), comment.Train)

	w.Header().Set("Content-Type", "application/json; charset=utf-8")
	w.Header().Set("Cache-Control", "no-store")
	json.NewEncoder(w).Encode(struct{}{})
}
