module git.autistici.org/ai/nospam2

go 1.19

require (
	github.com/agnivade/levenshtein v1.1.1
	github.com/blevesearch/segment v0.9.1
	github.com/boltdb/bolt v1.3.1
	github.com/google/subcommands v1.2.0
	github.com/miekg/dns v1.1.50
	github.com/prometheus/client_golang v1.11.0
	golang.org/x/net v0.5.0
	golang.org/x/text v0.6.0
	mvdan.cc/xurls/v2 v2.4.0
)

require (
	github.com/beorn7/perks v1.0.1 // indirect
	github.com/cespare/xxhash/v2 v2.1.1 // indirect
	github.com/golang/protobuf v1.4.3 // indirect
	github.com/matttproud/golang_protobuf_extensions v1.0.1 // indirect
	github.com/prometheus/client_model v0.2.0 // indirect
	github.com/prometheus/common v0.26.0 // indirect
	github.com/prometheus/procfs v0.6.0 // indirect
	golang.org/x/mod v0.6.0-dev.0.20220419223038-86c51ed26bb4 // indirect
	golang.org/x/sys v0.4.0 // indirect
	golang.org/x/tools v0.1.12 // indirect
	google.golang.org/protobuf v1.26.0-rc.1 // indirect
)
