package nospam

import (
	"context"
	"errors"
	"fmt"
	"log"
	"strings"
	"sync"

	"github.com/boltdb/bolt"
	"github.com/prometheus/client_golang/prometheus"
)

type Plugin interface {
	Name() string
	Init(map[string]interface{}, *bolt.DB) error
	Test(context.Context, *Comment) (float64, string, error)
	Train(context.Context, *Comment) error
}

var plugins []Plugin

func RegisterPlugin(p Plugin) {
	plugins = append(plugins, p)
}

func initDB(config map[string]interface{}) (*bolt.DB, error) {
	dbPath := "./nospam.db"
	if s, ok := config["db_path"].(string); ok && s != "" {
		dbPath = s
	}

	db, err := bolt.Open(dbPath, 0600, nil)
	if err != nil {
		return nil, err
	}

	// Set options on the database.
	if b, ok := config["db_no_fsync"].(bool); ok && b {
		db.NoSync = true
	}

	return db, nil
}

func initPlugins(config map[string]interface{}, db *bolt.DB) ([]Plugin, error) {
	var enabled []Plugin
	var enabledNames []string
	for _, p := range plugins {
		if err := p.Init(config, db); err != nil {
			log.Printf("plugin %s initialization error: %v", p.Name(), err)
			continue
		}
		enabled = append(enabled, p)
		enabledNames = append(enabledNames, p.Name())
	}
	log.Printf("enabled plugins: %s", strings.Join(enabledNames, ", "))
	return enabled, nil
}

// Engine for the NoSpam classification service.
type Engine struct {
	db                 *bolt.DB
	plugins            []Plugin
	customScores       map[string]float64
	spamThreshold      float64
	autolearnThreshold float64
}

const defaultScoreTag = "default"

var defaultScores = map[string]float64{
	defaultScoreTag: 1.0,
	"linkabuse":     0.7,
	"linkcount":     0.7,
	"badlink":       0.5,
	"nameisurl":     0.5,
	"weirdurls":     0.5,
	"freemail":      0.5,
	"nofollow":      0.3,
}

func getCustomScores(scoreMap map[string]interface{}) map[string]float64 {
	scores := defaultScores
	for k, v := range scoreMap {
		switch vv := v.(type) {
		case float64:
			scores[k] = vv
		case int:
			scores[k] = float64(vv)
		}
	}
	return scores
}

func New(config map[string]interface{}) (*Engine, error) {
	spamThreshold := 1.0
	if f, ok := config["spam_threshold"].(float64); ok && f > 0 {
		spamThreshold = f
	}

	autolearnThreshold := 1.5
	if f, ok := config["autolearn_threshold"].(float64); ok && f > 0 {
		autolearnThreshold = f
	}

	db, err := initDB(config)
	if err != nil {
		return nil, err
	}

	enabledPlugins, err := initPlugins(config, db)
	if err != nil {
		return nil, err
	}

	return &Engine{
		db:                 db,
		plugins:            enabledPlugins,
		spamThreshold:      spamThreshold,
		autolearnThreshold: autolearnThreshold,
		customScores:       getCustomScores(config),
	}, nil
}

func (s *Engine) Close() {
	s.db.Close()
}

// Train the engine with a new comment.
func (s *Engine) Train(ctx context.Context, comment *Comment) error {
	if comment.Comment == "" {
		return errors.New("empty comment")
	}
	if comment.Train != CategoryOk && comment.Train != CategorySpam {
		return errors.New("unknown train category")
	}

	classifyCounter.WithLabelValues(string(comment.Train)).Inc()

	return s.doTrain(ctx, comment, false)
}

func (s *Engine) doTrain(ctx context.Context, comment *Comment, autolearn bool) error {
	var errs []string
	for _, p := range s.plugins {
		if err := p.Train(ctx, comment); err != nil {
			errs = append(errs, fmt.Sprintf("%s: %v", p.Name(), err))
		}
	}
	if len(errs) > 0 {
		return errors.New(strings.Join(errs, "; "))
	}
	return nil
}

func (s *Engine) getScore(name string) float64 {
	score, ok := s.customScores[name]
	if !ok {
		score = s.customScores[defaultScoreTag]
	}
	return score
}

func (s *Engine) Test(ctx context.Context, comment *Comment) (*Result, error) {
	var wg sync.WaitGroup
	ch := make(chan *TestResult, len(plugins))

	// Fan out all the plugin tests in different goroutines.
	for _, p := range s.plugins {
		wg.Add(1)
		go func(p Plugin) {
			defer wg.Done()

			score, message, err := p.Test(ctx, comment)
			status := Status(StatusOk)
			if err != nil {
				status = StatusError
				message = err.Error()
				log.Printf("error in plugin %s: %v", p.Name(), err)
			} else if score > 0 {
				score *= s.getScore(p.Name())
			}
			ch <- &TestResult{
				Name:    p.Name(),
				Status:  status,
				Message: message,
				Score:   score,
			}
		}(p)
	}

	go func() {
		wg.Wait()
		close(ch)
	}()

	// Collect all the test results.
	var res Result
	for tres := range ch {
		// Only include non-zero results.
		if tres.Score == 0 {
			continue
		}
		res.Score += tres.Score
		res.Tests = append(res.Tests, tres)

		pluginMatches.WithLabelValues(tres.Name).Inc()
	}

	// Classify the message by comparing the overall score against
	// the threshold, and eventually trigger the auto-learn
	// feedback mechanism if the score is high enough.
	res.Category = CategoryOk
	if res.Score >= s.spamThreshold {
		res.Category = CategorySpam

		// Autolearn?
		if res.Score >= s.autolearnThreshold {
			go func() {
				comment.Train = res.Category
				if err := s.doTrain(ctx, comment, true); err != nil {
					log.Printf("autolearn error: %v", err)
				}
			}()
		}
	}

	testCounter.WithLabelValues(niceSite(comment.Site), string(res.Category)).Inc()

	return &res, nil
}

// Turn the 'site' field into something we can use as a Prometheus
// label (strip the schema prefix).
func niceSite(s string) string {
	switch {
	case strings.HasPrefix(s, "http://"):
		return s[7:]
	case strings.HasPrefix(s, "https://"):
		return s[8:]
	default:
		return s
	}
}

var (
	testCounter = prometheus.NewCounterVec(
		prometheus.CounterOpts{
			Name: "nospam_comments",
			Help: "Number of comments by category (cumulative counter).",
		},
		[]string{"site", "kind"},
	)
	classifyCounter = prometheus.NewCounterVec(
		prometheus.CounterOpts{
			Name: "nospam_classify",
			Help: "Number of comments sent for training (cumulative counter).",
		},
		[]string{"train"},
	)
	pluginMatches = prometheus.NewCounterVec(
		prometheus.CounterOpts{
			Name: "nospam_plugin_match",
			Help: "Spam hits by plugin name.",
		},
		[]string{"plugin"},
	)
)

func init() {
	prometheus.MustRegister(
		testCounter,
		classifyCounter,
		pluginMatches,
	)
}
