FROM golang:1.20 AS build
ADD . /src
WORKDIR /src
RUN env CGO_ENABLED=0 go build -ldflags="-w -s -extldflags=-static" -tags prod -o nospam ./cmd/nospam

FROM scratch
COPY --from=build /src/nospam /nospam

ENTRYPOINT ["/nospam"]

