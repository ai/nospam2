package integrationtest

import (
	"context"
	"io/ioutil"
	"os"
	"testing"

	nospam "git.autistici.org/ai/nospam2"
	_ "git.autistici.org/ai/nospam2/plugins"
)

func TestSetup(t *testing.T) {
	dir, err := ioutil.TempDir("", "")
	if err != nil {
		t.Fatal(err)
	}
	defer os.RemoveAll(dir)

	engine, err := nospam.New(map[string]interface{}{
		"db_path": dir + "/nospam.db",
	})
	if err != nil {
		t.Fatal(err)
	}

	comment := &nospam.Comment{
		Comment: "<strong>pharmacy</strong> But cheap vi4gra online.",
	}
	result, err := engine.Test(context.Background(), comment)
	if err != nil {
		t.Fatal(err)
	}
	t.Logf("%+v", result)
}
