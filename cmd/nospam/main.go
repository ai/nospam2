package main

import (
	"context"
	"encoding/json"
	"flag"
	"log"
	"os"

	"github.com/google/subcommands"

	// Import all plugins.
	_ "git.autistici.org/ai/nospam2/plugins"
)

type commandBase struct {
	configPath string
}

func (c *commandBase) SetFlags(f *flag.FlagSet) {
	f.StringVar(&c.configPath, "config", "", "configuration file `path`")
}

func (c *commandBase) readConfig() (map[string]interface{}, error) {
	if c.configPath == "" {
		//return nil, errors.New("Must specify --config")
		return make(map[string]interface{}), nil
	}
	f, err := os.Open(c.configPath)
	if err != nil {
		return nil, err
	}
	defer f.Close()

	var config map[string]interface{}
	if err := json.NewDecoder(f).Decode(&config); err != nil {
		return nil, err
	}
	return config, nil
}

func init() {
	subcommands.Register(subcommands.HelpCommand(), "help")
	subcommands.Register(subcommands.CommandsCommand(), "help")
	subcommands.Register(subcommands.FlagsCommand(), "help")
}

func main() {
	flag.Parse()
	log.SetFlags(0)

	os.Exit(int(subcommands.Execute(context.Background())))
}
