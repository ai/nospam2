package main

import (
	"bufio"
	"encoding/json"
	"io"

	nospam "git.autistici.org/ai/nospam2"
)

type recordReader struct {
	*bufio.Scanner
}

func newRecordReader(r io.Reader) *recordReader {
	return &recordReader{
		Scanner: bufio.NewScanner(r),
	}
}

func (r *recordReader) Record() (*nospam.Comment, error) {
	var comment nospam.Comment
	if err := json.Unmarshal(r.Scanner.Bytes(), &comment); err != nil {
		return nil, err
	}
	return &comment, nil
}
