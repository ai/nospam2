package main

import (
	"context"
	"flag"
	"log"
	"net"
	"net/http"
	_ "net/http/pprof"
	"os"
	"os/signal"
	"syscall"
	"time"

	nospam "git.autistici.org/ai/nospam2"
	"github.com/google/subcommands"
	"github.com/prometheus/client_golang/prometheus/promhttp"
)

type serverCommand struct {
	commandBase

	addr    string
	timeout time.Duration
}

func (c *serverCommand) Name() string     { return "server" }
func (c *serverCommand) Synopsis() string { return "run the API server" }
func (c *serverCommand) Usage() string {
	return `server [<flags>]
        Start the HTTP API server.

`
}

func (c *serverCommand) SetFlags(f *flag.FlagSet) {
	c.commandBase.SetFlags(f)
	f.StringVar(&c.addr, "addr", ":9001", "address to listen on")
	f.DurationVar(&c.timeout, "timeout", 10*time.Second, "request timeout")
}

func (c *serverCommand) Execute(ctx context.Context, _ *flag.FlagSet, _ ...interface{}) subcommands.ExitStatus {
	config, err := c.readConfig()
	if err != nil {
		log.Printf("error reading configuration: %v", err)
		return subcommands.ExitFailure
	}

	engine, err := nospam.New(config)
	if err != nil {
		log.Printf("engine initialization failure: %v", err)
		return subcommands.ExitFailure
	}
	defer engine.Close()

	if err := c.runServer(ctx, engine); err != nil {
		log.Printf("error: %v", err)
		return subcommands.ExitFailure
	}

	return subcommands.ExitSuccess
}

// Just a nicer function to run a Context-controlled http.Server.
func (c *serverCommand) runServer(ctx context.Context, engine *nospam.Engine) error {
	// Build the http.Server.
	mux := http.NewServeMux()
	mux.Handle("/debug/", http.DefaultServeMux)
	mux.Handle("/metrics", promhttp.Handler())
	mux.Handle("/", engine)
	srv := &http.Server{
		Handler:           http.TimeoutHandler(mux, c.timeout, ""),
		ReadHeaderTimeout: 10 * time.Second,
		IdleTimeout:       1800 * time.Second,
	}

	// Install a signal handler that will terminate the server.
	sigCh := make(chan os.Signal, 1)
	go func() {
		<-sigCh
		log.Printf("exiting")

		// Attempt to gracefully terminate, then shut down
		// remaining clients.
		sctx, cancel := context.WithTimeout(ctx, 5*time.Second)
		defer cancel()
		if err := srv.Shutdown(sctx); err == context.Canceled {
			if err := srv.Close(); err != nil {
				log.Printf("error terminating server: %v", err)
			}
		}
	}()

	signal.Notify(sigCh, syscall.SIGINT, syscall.SIGTERM)

	// Create the net.Listener separately, so we can detect
	// initialization-time errors safely.
	l, err := net.Listen("tcp", c.addr)
	if err != nil {
		return err
	}

	log.Printf("listening on %s", c.addr)
	err = srv.Serve(l)
	if err == http.ErrServerClosed {
		// This error is expected on shutdown, don't log it.
		err = nil
	}
	return err
}

func init() {
	subcommands.Register(&serverCommand{}, "")
}
