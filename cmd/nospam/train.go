package main

import (
	"context"
	"flag"
	"log"
	"os"
	"runtime"
	"sync"
	"time"

	nospam "git.autistici.org/ai/nospam2"
	"github.com/google/subcommands"
)

type trainCommand struct {
	commandBase
}

func (c *trainCommand) Name() string     { return "train" }
func (c *trainCommand) Synopsis() string { return "train the engine" }
func (c *trainCommand) Usage() string {
	return `train [<flags>]
        Train the engine, expects JSONL data in input.

`
}

func (c *trainCommand) SetFlags(f *flag.FlagSet) {
	c.commandBase.SetFlags(f)
}

func trainWorker(ch <-chan *nospam.Comment, engine *nospam.Engine) {
	for comment := range ch {
		if err := engine.Train(context.Background(), comment); err != nil {
			log.Printf("training error: %v", err)
		}
	}
}

func (c *trainCommand) Execute(ctx context.Context, _ *flag.FlagSet, _ ...interface{}) subcommands.ExitStatus {
	config, err := c.readConfig()
	if err != nil {
		log.Printf("error reading configuration: %v", err)
		return subcommands.ExitFailure
	}

	// Set NoSync on the database, makes training 5x faster.
	config["db_no_fsync"] = true

	engine, err := nospam.New(config)
	if err != nil {
		log.Printf("engine initialization failure: %v", err)
		return subcommands.ExitFailure
	}
	defer engine.Close()

	// Spread the training across many threads, to take advantage
	// of CPU parallelism (though the bottleneck is still going to
	// be BoltDB's write performance).
	ch := make(chan *nospam.Comment, 100)
	var wg sync.WaitGroup
	for i := 0; i < runtime.NumCPU(); i++ {
		wg.Add(1)
		go func() {
			trainWorker(ch, engine)
			wg.Done()
		}()
	}

	start := time.Now()
	var numRecords int

	rr := newRecordReader(os.Stdin)
	for rr.Scan() {
		comment, err := rr.Record()
		if err != nil {
			log.Printf("error reading record: %v", err)
			continue
		}
		ch <- comment
		numRecords++
	}

	close(ch)
	wg.Wait()
	elapsed := time.Since(start)

	if err := rr.Err(); err != nil {
		log.Printf("error reading input: %v", err)
		return subcommands.ExitFailure
	}

	log.Printf("trained %d records in %s seconds (%g recs/s)", numRecords, elapsed, float64(numRecords)/elapsed.Seconds())

	return subcommands.ExitSuccess
}

func init() {
	subcommands.Register(&trainCommand{}, "")
}
