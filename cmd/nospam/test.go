package main

import (
	"context"
	"flag"
	"fmt"
	"log"
	"os"
	"runtime"
	"sync"

	nospam "git.autistici.org/ai/nospam2"
	"github.com/google/subcommands"
)

type testCommand struct {
	commandBase
}

func (c *testCommand) Name() string     { return "test" }
func (c *testCommand) Synopsis() string { return "run the engine with test input" }
func (c *testCommand) Usage() string {
	return `test [<flags>]
        Test the engine, expects JSONL data in input, provides 
        validation statistics on output.

`
}

func (c *testCommand) SetFlags(f *flag.FlagSet) {
	c.commandBase.SetFlags(f)
}

type statsKey struct {
	actual, expected string
}

type stats struct {
	mx    sync.Mutex
	stats map[statsKey]int
}

func newStats() *stats {
	return &stats{
		stats: make(map[statsKey]int),
	}
}

func (s *stats) inc(actual, expected nospam.Category) {
	s.mx.Lock()
	key := statsKey{string(actual), string(expected)}
	s.stats[key]++
	s.mx.Unlock()
}

func testWorker(ch <-chan *nospam.Comment, engine *nospam.Engine, stats *stats) {
	for comment := range ch {
		res, err := engine.Test(context.Background(), comment)
		if err != nil {
			log.Printf("error: %v", err)
			continue
		}

		// Convert the nospam result to the "train" field format.
		cat := res.Category
		if cat == "" {
			continue
		}

		if cat != comment.Train {
			fmt.Printf("\nmis-classified comment (was %s, classified as %s):\n", comment.Train, cat)
			fmt.Println(comment.String())
			fmt.Println(res.String())
		}

		stats.inc(cat, comment.Train)
	}
}

func (c *testCommand) Execute(ctx context.Context, _ *flag.FlagSet, _ ...interface{}) subcommands.ExitStatus {
	config, err := c.readConfig()
	if err != nil {
		log.Printf("error reading configuration: %v", err)
		return subcommands.ExitFailure
	}

	engine, err := nospam.New(config)
	if err != nil {
		log.Printf("engine initialization failure: %v", err)
		return subcommands.ExitFailure
	}
	defer engine.Close()

	stats := newStats()

	ch := make(chan *nospam.Comment, 100)
	var wg sync.WaitGroup
	for i := 0; i < runtime.NumCPU(); i++ {
		wg.Add(1)
		go func() {
			testWorker(ch, engine, stats)
			wg.Done()
		}()
	}

	rr := newRecordReader(os.Stdin)
	for rr.Scan() {
		comment, err := rr.Record()
		if err != nil {
			log.Printf("error reading record: %v", err)
			continue
		}
		ch <- comment
	}

	close(ch)
	wg.Wait()

	if err := rr.Err(); err != nil {
		log.Printf("error reading input: %v", err)
		return subcommands.ExitFailure
	}

	fmt.Println(stats.stats)

	return subcommands.ExitSuccess
}

func init() {
	subcommands.Register(&testCommand{}, "")
}
